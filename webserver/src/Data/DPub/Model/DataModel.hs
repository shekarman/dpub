{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Data.DPub.Model.DataModel where

import Data.Text
import Data.Time
import Data.DPub.Model.EnumeratedTypes
import Database.Persist.TH



-- TODO : Read from a file so we can share the model across database
-- types: postgres, mysql etc.
share [mkPersist sqlSettings, mkMigrate "migrateAll"]
 [persistLowerCase|
    Message
      text Text
      insertTime UTCTime default = CURRENT_TIMESTAMP
      deriving Show
    Article json -- using singulars as per db conventions. 
      -- Id Text -- Can I rename this to Id ?
      title Text 
      author AuthorId
      freeContentAddress Text
      contentAddress Text
      state ArticleState
      insertTime UTCTime default = CURRENT_TIMESTAMP
      updateTime UTCTime default = CURRENT_TIMESTAMP
      deriving Show 
    Review json 
      article ArticleId 
      source ArticleId 
      title Text 
      author Text 
      contentAddress Text 
      state ArticleState
      deriving Show 
    ArticleStateChange json 
      article ArticleId 
      state ArticleState 
      audit UTCTime default = CURRENT_TIMESTAMP
      deriving Show 
    ArticleCost json 
      article ArticleId 
      costType CostType
      costUnit CostUnit 
      coinType CoinType
      currency Text 
      costValue Text
      deriving Show
    Author json 
      article ArticleId 
      user UserId 
    Editors json 
      article ArticleId
      user UserId 
    Translators json 
      article ArticleId 
      user UserId
      deriving (Show)
    Buyers json 
      article ArticleId 
      user UserId
      deriving (Show) 
    User json 
      address AddressId 
      userBio Text
      writingCapabilities Text
      editingCapabilities Text
      translationCapabilities Text 
      interests Text 
      deriving (Show)
    Language json 
      isoThreeLetterCode Text 
      isoDescription Text 
      deriving (Show)
    UserLanguages json 
      user UserId 
      languageId LanguageId 
    AccountsReceivable json 
      userId UserId 
      amountType Text -- ??
      amountUnit Text -- ??
      coinType Text -- ??
      coinUnit Text -- ??
      currency Text -- ?? 
      amount Text -- ??
      deriving Show 
    AccountsPayable json 
      userId UserId 
      amountType Text -- ??
      amountUnit Text -- ??
      coinType Text -- ??
      coinUnit Text -- ??
      currency Text -- ?? 
      amount Text -- ??
      deriving Show
    Address json 
      address Text -- the address on the block.
|]

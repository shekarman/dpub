{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
{-# LANGUAGE DeriveGeneric              #-}

module Data.DPub.Model.EnumeratedTypes where

import Database.Persist.TH
import GHC.Generics
import Data.Aeson


data ArticleState = 
    Empty | Writing | SolicitingEditors
    | Editing 
    | EditComplete 
    | AwaitEditApproval 
    | Published
    | ForSale
    | ArchivedOrDeleted -- Nothing gets deleted.
    deriving (Show, Read, Eq, Generic)

derivePersistField "ArticleState"

data CostType =
    Edit | Translate | Print
    | ISBNRegister | Resell
    | Advertise | SalePrice 
    | Commission
    deriving (Show, Read, Eq, Generic)
derivePersistField "CostType"

data CostUnit = CoinType | FiatCurrency
  deriving (Show, Read, Eq, Generic)
derivePersistField "CostUnit"

data CoinType = 
  BitCoin | Ether | LiteCoin | FileCoin | Ada | DPubCoin
  deriving (Show, Read, Eq, Generic)
derivePersistField "CoinType"

instance ToJSON ArticleState
instance FromJSON ArticleState
instance ToJSON CostType
instance FromJSON CostType
instance ToJSON CostUnit 
instance FromJSON CostUnit 
instance ToJSON CoinType
instance FromJSON CoinType
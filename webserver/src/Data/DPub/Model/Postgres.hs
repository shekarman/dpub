{-# LANGUAGE EmptyDataDecls             #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Data.DPub.Model.Postgres where

import Control.Monad.IO.Class(liftIO)
import Control.Monad.Logger(runStderrLoggingT)
import Control.Monad.Trans.Reader
import Data.ByteString.Char8
import Data.Monoid((<>))
import Data.Text
import Data.Time
import Data.DPub.Model.DataModel 
import Data.DPub.Model.EnumeratedTypes
import Database.Persist
import Database.Persist.Postgresql
import Database.Persist.TH


c8Pack :: String -> ByteString
c8Pack = Data.ByteString.Char8.pack

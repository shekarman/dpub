
{-# LANGUAGE DeriveDataTypeable, TemplateHaskell, DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts, OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE QuasiQuotes, TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
-- A webservice for D Pub

module Network.DPub.Server.RestServerAPI where

import Control.Monad.IO.Class (liftIO)
import Data.Aeson
import Data.Text as Text
import Network.DPub.Server.Article
import Network.DPub.Server.Common
import Network.DPub.Server.Language
import Network.DPub.Server.Account
import Network.DPub.Server.User
import Network.DPub.Server.Review
import Data.DPub.Model.DataModel
import Database.Persist.Sqlite
import Database.Persist.TH 
import Yesod

instance Yesod DPub


mkYesod "DPub" [parseRoutes|
/ HomeR GET
/languages LanguagesR GET 
/addLanguage AddLanguageR POST
/deleteLanguage DeleteLanguageR POST 
/addArticle AddArticleR POST 
/deleteArticle DeleteArticleR POST 
/addReview AddReviewR POST 
/deleteReview DeleteReviewR POST
/addArticleState AddArticleStateR POST
/deleteArticleState DeleteArticleStateR POST 
/addArticleCost AddArticleCostR POST 
/deleteArticleCost DeleteArticleCostR POST
/addAuthor AddAuthorR POST 
/deleteAuthor DeleteAuthorR POST 
/addEditor AddEditorR POST 
/deleteEditor DeleteEditorR POST
/addTranslator AddTranslatorR POST
/deleteTranslator DeleteTranslatorR POST 
/addBuyer AddBuyerR POST 
/deleteBuyer DeleteBuyerR POST 
/addUser AddUserR POST 
/deleteUser DeleteUserR POST 
/addUserLanguage AddUserLanguageR POST 
/addAccountsReceivable AddAccountsReceivableR POST
/deleteAccountsReceivable DeleteAccountsReceivableR POST
/addAccountsPayable AddAccountsPayableR POST 
/deleteAccountsPayable DeleteAccountsPayableR POST
|]



getHomeR :: YesodHandler Value
getHomeR = return $ String "Welcome"

restMain :: IO () 
restMain = do 
  saveLanguage $ Language "ENG" "English"
  warp 3000 $ DPub 123


{-# LANGUAGE DeriveDataTypeable, TemplateHaskell, DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts, OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE QuasiQuotes, TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Network.DPub.Server.Article where 

import Control.Monad.IO.Class (liftIO)
import Data.Text as Text
import Data.Aeson
import Database.Persist.Sqlite
import Database.Persist.TH 
import Yesod
import Data.DPub.Model.DataModel
import Network.DPub.Server.Common


postAddArticleR :: YesodHandler ()
postAddArticleR = undefined 

postDeleteArticleR :: YesodHandler ()
postDeleteArticleR = undefined
postUpdateArticleR :: YesodHandler ()
postUpdateArticleR = undefined

postAddArticleCostR :: YesodHandler ()
postAddArticleCostR = undefined 

postDeleteArticleCostR :: YesodHandler () 
postDeleteArticleCostR = undefined

postAddArticleStateR :: YesodHandler() 
postAddArticleStateR = undefined

postDeleteArticleStateR :: YesodHandler ()
postDeleteArticleStateR = undefined 
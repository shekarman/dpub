{-# LANGUAGE DeriveDataTypeable, TemplateHaskell, DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts, OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE QuasiQuotes, TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Network.DPub.Server.User where

import Control.Monad.IO.Class (liftIO)
import Data.Text as Text
import Data.Aeson
import Database.Persist.Sqlite
import Database.Persist.TH 
import Yesod
import Data.DPub.Model.DataModel
import Network.DPub.Server.Common

postDeleteUserR :: YesodHandler ()
postDeleteUserR = undefined

postAddUserR :: YesodHandler() 
postAddUserR = undefined

postAddBuyerR :: YesodHandler ()
postAddBuyerR = undefined 

postDeleteBuyerR :: YesodHandler ()
postDeleteBuyerR = undefined

postAddTranslatorR :: YesodHandler ()
postAddTranslatorR = undefined 

postDeleteTranslatorR :: YesodHandler ()
postDeleteTranslatorR = undefined

postAddEditorR :: YesodHandler ()
postAddEditorR = undefined 

postDeleteEditorR :: YesodHandler ()
postDeleteEditorR = undefined

postAddAuthorR :: YesodHandler ()
postAddAuthorR = undefined 

postDeleteAuthorR :: YesodHandler ()
postDeleteAuthorR = undefined

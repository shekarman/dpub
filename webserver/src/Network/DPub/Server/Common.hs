module Network.DPub.Server.Common where
import Yesod

data DPub = DPub {_unApp :: Int} deriving (Show)
type YesodHandler = HandlerT DPub IO

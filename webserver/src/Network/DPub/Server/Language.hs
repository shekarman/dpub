{-# LANGUAGE DeriveDataTypeable, TemplateHaskell, DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts, OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE QuasiQuotes, TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

module Network.DPub.Server.Language where

import Control.Monad.IO.Class (liftIO)
import Data.Text as Text
import Data.Aeson
import Database.Persist.Sqlite
import Yesod
import Data.DPub.Model.DataModel
import Network.DPub.Server.Common


databaseString :: Text
databaseString = "test.db"

jsonType :: ContentType 
jsonType = "application/json"

instance ToContent Language where 
  toContent (Language x y) = toContent $ toJSON (Language x y)

instance ToTypedContent Language where 
  toTypedContent l@(Language x y) =
      TypedContent jsonType (toContent l)

instance ToContent UserLanguages where 
  toContent l@(UserLanguages userId languageId) = 
    toContent $ toJSON l

instance ToTypedContent UserLanguages where 
  toTypedContent l@(UserLanguages _ _ ) =
    TypedContent jsonType $ toContent l

saveLanguage :: (PersistEntityBackend record ~ SqlBackend,
                 PersistEntity record, MonadIO m, MonadBaseControl IO m) =>
                record -> m Text
saveLanguage aLanguage = 
  runSqlite databaseString $ do 
    runMigration migrateAll -- To fix the type issue.
    l <- insert aLanguage
    return $ pack . show $ l

deleteLanguage :: (MonadBaseControl IO m, MonadIO m) => Language -> m Language
deleteLanguage aLanguage@(Language isoCode _) = 
  runSqlite databaseString $ do 
    runMigration migrateAll
    _ <- deleteWhere ([LanguageIsoThreeLetterCode ==. isoCode] :: [Filter Language])
    return aLanguage

saveUserLanguage :: (MonadBaseControl IO m, MonadIO m ) => UserLanguages -> m UserLanguages
saveUserLanguage aUserLanguage@(UserLanguages _ _ ) = do 
  runSqlite databaseString $ do 
    runMigration migrateAll
    _ <- insert aUserLanguage 
    return aUserLanguage

queryLanguages :: IO [Entity Language]
queryLanguages  = 
  runSqlite "test.db" $ do 
    runMigration migrateAll
    selectList [] []

getLanguagesR :: YesodHandler Value 
getLanguagesR = do
  allLanguages <- liftIO $ fmap toJSON queryLanguages
  return $ object ["languages" .= allLanguages]

postAddLanguageR :: YesodHandler Text
postAddLanguageR = do 
  lang <- requireJsonBody :: YesodHandler Language 
  _ <- saveLanguage lang 
  return $ Text.pack $ show lang


postDeleteLanguageR :: YesodHandler Language 
postDeleteLanguageR = do 
  lang <- requireJsonBody :: YesodHandler Language 
  _ <- deleteLanguage lang 
  return lang

postAddUserLanguageR :: YesodHandler UserLanguages
postAddUserLanguageR = do 
  addUserLang <- requireJsonBody :: YesodHandler UserLanguages 
  _ <- liftIO $ saveUserLanguage addUserLang
  return addUserLang


var Elm = require('./Program.elm');
require('./articles.css');

var elmApp = Elm.Program.embed(document.getElementById('elmDiv'), { baseUrl:'http://localhost:8008'} );

elmApp.ports.purchaseArticle.subscribe(function(article) {
  alert('Article ' + article.title + ' will be displayed soon.')
})

module Article exposing (..)

import Http
import Json.Decode exposing (..)
import Json.Decode.Pipeline exposing (decode, hardcoded, optional, required)
import Json.Encode as Jsone
import Ui.Ratings

type alias Model = { users : List User, editors : List Editor, printers : List Printer, articles : List Article, currentUserName : String }

type Msg =
    ArticlesRetrieved (Result Http.Error (List Article))
    | BuyArticle Article
    | PreviewArticle Article
    | UploadArticle User
    | RequestEditors Article
    | RequestTranslators Article
    | RequestPrinters Article
    | PublishArticle Article
    | BidForEditing Article
    | BidForTranslating Article
    | BidForPrinting Article
    | SubmitForApproval Article
    | DeleteArticle Article
    | ApproveEdits Article
    | ApproveTranslation Article
    | ApprovePrint Article
    | EditInProgress Article
    | EditComplete Article
    | TranslationInProgress Article
    | TranslationComplete Article
    | PrintInProgress Article
    | PrintComplete Article


type alias Flags = { baseUrl : String }

type ArticleState
    = EMPTY
    | WRITING
    | EDITING
    | EPUBLISHED
    | PRINTING
    | PUBLISHED
    | DELETED

articleStateDecoder : Decoder ArticleState
articleStateDecoder =
    string
        |> andThen (\str ->
                    case str of
                        "EMPTY" -> succeed EMPTY
                        "WRITING" -> succeed WRITING
                        "EDITING" -> succeed EDITING
                        "EPUBLISHED" -> succeed EPUBLISHED
                        "PUBLISHED" -> succeed PUBLISHED
                        "PRINTING" -> succeed PRINTING
                        "DELETED" -> succeed DELETED
                        somethingElse -> fail <| "Unknown articleState: " ++ somethingElse
                   )

type EditingState
    =  SOLICITING_EDITORS
    | EDIT_IN_PROGRESS
    | EDIT_COMPLETE
    | AWAIT_EDIT_APPROVAL
    | EDITED

editingStateDecoder : Decoder EditingState
editingStateDecoder =
    string
        |> andThen (\str ->
                    case str of
                        "SOLICITING_EDITORS" -> succeed SOLICITING_EDITORS
                        "EDIT_IN_PROGRESS" -> succeed EDIT_IN_PROGRESS
                        "EDIT_COMPLETE" -> succeed EDIT_COMPLETE
                        "AWAIT_EDIT_APPROVAL" -> succeed AWAIT_EDIT_APPROVAL
                        "EDITED" -> succeed EDITED
                        somethingElse -> fail <| "Unknown editingState: " ++ somethingElse
                   )

type PrintingState
    = SOLICITING_PRINTERS
    | PRINT_IN_PROGRESS
    | PRINT_COMPLETE
    | SHIPPED_FOR_APPROVAL
    | AWAIT_PRINT_APPROVAL
    | PRINTED

printingStateDecoder : Decoder PrintingState
printingStateDecoder =
    string
        |> andThen (\str ->
                    case str of
                        "SOLICITING_PRINTERS" -> succeed SOLICITING_PRINTERS
                        "PRINT_IN_PROGRESS" -> succeed PRINT_IN_PROGRESS
                        "PRINT_COMPLETE" -> succeed PRINT_COMPLETE
                        "SHIPPED_FOR_APPROVAL" -> succeed SHIPPED_FOR_APPROVAL
                        "AWAIT_PRINT_APPROVAL" -> succeed AWAIT_PRINT_APPROVAL
                        "PRINTED" -> succeed PRINTED
                        somethingElse -> fail <| "Unknown printingState: " ++ somethingElse
                   )

type CostType
    = EDIT
    | TRANSLATE
    | PRINT
    | ISBN_REGISTER
    | RESELL
    | ADVERTISE
    | SALE_PRICE
    | DPUB_COMMISSION
    | OTHER

costTypeDecoder : Decoder CostType
costTypeDecoder =
    string
        |> andThen (\str ->
                    case str of
                        "EDIT" -> succeed EDIT
                        "TRANSLATE" -> succeed TRANSLATE
                        "PRINT" -> succeed PRINT
                        "ISBN_REGISTER" -> succeed ISBN_REGISTER
                        "RESELL" -> succeed RESELL
                        "ADVERTISE" -> succeed ADVERTISE
                        "SALE_PRICE" -> succeed SALE_PRICE
                        "DPUB_COMMISSION" -> succeed DPUB_COMMISSION
                        "OTHER" -> succeed OTHER
                        somethingElse -> fail <| "Unknown costType: " ++ somethingElse
                   )


type RatingType
    = ONE_STAR
    | TWO_STAR
    | THREE_STAR
    | FOUR_STAR
    | FIVE_STAR

ratingTypeDecoder : Decoder RatingType
ratingTypeDecoder =
    string
        |> andThen (\str ->
                    case str of
                        "ONE_STAR" -> succeed ONE_STAR
                        "TWO_STAR" -> succeed TWO_STAR
                        "THREE_STAR" -> succeed THREE_STAR
                        "FOUR_STAR" -> succeed FOUR_STAR
                        "FIVE_STAR" -> succeed FIVE_STAR
                        somethingElse -> fail <| "Unknown ratingType: " ++ somethingElse
                   )


type CostUnit
    = COIN
    | FIAT_CURRENCY

costUnitDecoder : Decoder CostUnit
costUnitDecoder =
    string
        |> andThen (\str ->
                    case str of
                        "COIN" -> succeed COIN
                        "FIAT_CURRENCY" -> succeed FIAT_CURRENCY
                        somethingElse -> fail <| "Unknown costUnit: " ++ somethingElse
                   )

type CoinType
    = NOT_APPLICABLE
    | BITCOIN
    | ETHER
    | LITECOIN
    | FILECOIN
    | DPUB_COIN

coinTypeDecoder : Decoder CoinType
coinTypeDecoder =
    string
        |> andThen (\str ->
                    case str of
                        "NOT_APPLICABLE" -> succeed NOT_APPLICABLE
                        "BITCOIN" -> succeed BITCOIN
                        "ETHER" -> succeed ETHER
                        "LITECOIN" -> succeed LITECOIN
                        "FILECOIN" -> succeed FILECOIN
                        "DPUB_COIN" -> succeed DPUB_COIN
                        somethingElse -> fail <| "Unknown coinType: " ++ somethingElse
                   )

type alias Editor = {
        userID : Int,
        editingState : EditingState
    }


editorDecoder : Decoder Editor
editorDecoder =
    decode Editor
        |> required "userID" int
        |> required "editingState" editingStateDecoder

              
editorEncoder : Editor -> Jsone.Value
editorEncoder editor =
    Jsone.object
        [ ( "userID", Jsone.int editor.userID )
        , ( "editingState", Jsone.string <| toString editor.editingState )
        ]

type alias Printer = {
        userID : Int,
        printingState : PrintingState    
    }

printerDecoder : Decoder Printer
printerDecoder =
    decode Printer
        |> required "userID" int
        |> required "printingState" printingStateDecoder

              
printerEncoder : Printer -> Jsone.Value
printerEncoder printer =
    Jsone.object
        [ ( "userID", Jsone.int printer.userID )
        , ( "printingState", Jsone.string <| toString printer.printingState )
        ]

type alias Article
    = {
        articleID : Int,
        ownerID : Int,
        title : String,
        author : String,
        freeContentAddress : String,
        contentAddress : String,
        language : String,
        articleState : ArticleState,
        editors : List Editor,
        printers : List Printer,
        rating : RatingType,
        timeUpdated : Int
    }

articleDecoder : Decoder Article
articleDecoder =
    decode Article
        |> required "articleID" int
        |> required "ownerID" int
        |> required "title" string
        |> required "author" string
        |> required "freeContentAddress" string
        |> required "contentAddress" string
        |> required "language" string
        |> required "articleState" articleStateDecoder
        |> required "editors" (list editorDecoder)
        |> required "printers" (list printerDecoder)
        |> required "rating" ratingTypeDecoder
        |> required "timeUpdated" int


articleEncoder : Article -> Jsone.Value
articleEncoder article =
    Jsone.object
        [ ( "articleID", Jsone.int article.articleID )
        , ( "ownerID", Jsone.int article.ownerID )
        , ( "title", Jsone.string article.title )
        , ( "author", Jsone.string article.author )
        , ( "freeContentAddress", Jsone.string article.freeContentAddress )
        , ( "contentAddress", Jsone.string article.contentAddress )
        , ( "language", Jsone.string article.language )
        , ( "articleState", Jsone.string <| toString article.articleState )
        , ( "rating", Jsone.string <| toString article.rating )
        ]


type alias Review
    = {
        review : Article,
        sourceArticleID : Int
    }

reviewDecoder : Decoder Review
reviewDecoder =
    decode Review
        |> required "review" articleDecoder
        |> required "sourceArticleID" int


reviewEncoder : Review -> Jsone.Value
reviewEncoder review =
    Jsone.object
        [ ( "article", articleEncoder review.review )
        , ( "sourceArticleID", Jsone.int review.sourceArticleID )
        ]

type alias Cost
    = {
        articleID : Int,
        costType : CostType,
        costUnit : CostUnit,
        coinType : CoinType,
        currency : String,
        costValue : Float
    }

costDecoder : Decoder Cost
costDecoder =
    decode Cost
        |> required "articleID" int
        |> required "costType" costTypeDecoder
        |> required "costUnit" costUnitDecoder
        |> required "coinType" coinTypeDecoder
        |> required "currency" string
        |> required "costValue" float

costEncoder : Cost -> Jsone.Value
costEncoder cost =
    Jsone.object
        [ ( "articleID", Jsone.int cost.articleID )
        , ( "costType", Jsone.string <| toString cost.costType )
        , ( "costUnit", Jsone.string <| toString cost.costUnit )
        , ( "coinType", Jsone.string <| toString cost.coinType )
        , ( "currency", Jsone.string <| toString cost.currency )
        , ( "costValue", Jsone.string <| toString cost.costValue )
        ]

type alias User
    = {
        userID : Int,
        userName : String,
        userAddress : String,
        articlesWritten : Int,
        articlesPublished : Int,
        articlesEdited : Int,
        articlesTranslated : Int,
        articlesBought : Int,
        writingCapabilities : String,
        editingCapabilities : String,
        translationCapabilities : String,
        languages : String,
        userBio : String
    }

userDecoder : Decoder User
userDecoder =
    decode User
        |> required "userID" int
        |> required "userName" string
        |> required "userAddress" string
        |> required "articlesWritten" int
        |> required "articlesPublished" int
        |> required "articlesEdited" int
        |> required "articlesTranslated" int
        |> required "articlesBought" int
        |> required "writingCapabilities" string
        |> required "editingCapabilities" string
        |> required "translationCapabilities" string
        |> required "languages" string
        |> required "userBio" string

userEncoder : User -> Jsone.Value
userEncoder user =
    Jsone.object
        [ ( "userID", Jsone.int user.userID )
        , ( "userAddress", Jsone.string user.userAddress )
        , ( "articlesWritten", Jsone.int user.articlesWritten )
        , ( "articlesPublished", Jsone.int user.articlesPublished )
        , ( "articlesEdited", Jsone.int user.articlesEdited )
        , ( "articlesTranslated", Jsone.int user.articlesTranslated )
        , ( "articlesBought", Jsone.int user.articlesBought )
        , ( "writingCapabilities", Jsone.string user.writingCapabilities )
        , ( "editingCapabilities", Jsone.string user.editingCapabilities )
        , ( "translationCapabilities", Jsone.string user.translationCapabilities )
        , ( "languages", Jsone.string user.languages )
        , ( "userBio", Jsone.string user.userBio )
        ]


nullEncoder : (a -> Jsone.Value) -> Maybe a -> Jsone.Value
nullEncoder encoder val =
    case val of
        Just v ->
            encoder v

        Nothing ->
            Jsone.null

getUserById : (List User) -> Int -> User
getUserById users id =
    let
        l = List.filter (\u -> u.userID == id) users
    in
        case List.head l of
            Nothing ->
                { userID = -1,
                  userName = "",
                  userAddress = "",
                  articlesWritten = 0,
                  articlesPublished = 0,
                  articlesEdited = 0,
                  articlesTranslated = 0,
                  articlesBought = 0,
                  writingCapabilities = "",
                  editingCapabilities = "",
                  translationCapabilities = "",
                  languages = "",
                  userBio = ""
                }

            Just item -> item


getUserByName : (List User) -> String -> User
getUserByName users name =
    let
        l = List.filter (\u -> u.userName == name) users
    in
        case List.head l of
            Nothing ->
                { userID = -1,
                  userName = "",
                  userAddress = "",
                  articlesWritten = 0,
                  articlesPublished = 0,
                  articlesEdited = 0,
                  articlesTranslated = 0,
                  articlesBought = 0,
                  writingCapabilities = "",
                  editingCapabilities = "",
                  translationCapabilities = "",
                  languages = "",
                  userBio = ""
                }

            Just item -> item


{-
Actions that a user wants to take.

A. Author actions and pre-condition for each action.
   1. Upload an article
   2. Update an article (1)
   3. Define financial and legal terms
   4. Solicit editors (3)
   5. Assign editors (4)
   6. Approve edits with comments (5, x)
   7. Approve final edits (5, x)
   8. Solicit translator (4)
   9. Approve translator (8)
   10. Approve translation conditionally (9, x)
   11. Approve final translation (9, x)
   12. Solicit printers (3)
   13. Approve print proof with comments (12, x)
   14. Approve proof for print (12, x)
   15. Publish (3)
   16. Mark article for sale (15)

B. Editor actions and pre-conditions for each action
   1. Submit bid for editing
   2. Accept terms if approved for editing (1)
   3. Upload edited article (2)

C. Translator actions and pre-conditions for each action
   1. Submit bid for translating
   2. Accept terms if approved for translating (1)
   3. Upload translated article (2)

D. Printer actions and pre-conditions for each action
   1. Submit bid for printing
   2. Accept terms if approved for translating (1)
   3. "Ship printed proof" and notify author (2)
   4. Accept print orders and "start printing."
   5. "Ship printed copy" and notify author and reader

E. Reader actions and pre-conditins for each action
   1. Submit order for purchase (e-artile and/or printed copy).
   2. Download e-article. 
   3. View status of shipped order.
   4. Acknowledge receipt of order for physical article -- acknowledge should be time bound.
-}

authorActions = [
 "Upload",
 "Update",
 "Define terms",
 "Solicit editors",
 "Assign editors",
 "Approve edits with comments",
 "Approve edits final",
 "Solicit translators",
 "Approve translation with comments",
 "Approve translation final",
 "Solicit printers",
 "Approve proof with comments",
 "Approve proof final",
 "Publish",
 "Sell"]
    
editorActions = [
 "Submit bid for editing",
 "Accept terms and edit",
 "Upload edits"]

translatorActions = [
 "Submit bid for translating",
 "Accept terms and translate",
 "Upload translation"]

printerActions = [
 "Submit bid for printing",
 "Accept terms and print",
 "Notify proof shipment",
 "Accept print order",
 "Notify order shipment"]

readerActions = [
 "Buy",
 "Download",
 "View shipment status",
 "Acknowledge receipt of order"]

getActions : Article -> String -> List String
getActions article userName =
    if article.author == userName then
        authorActions
    else
        editorActions ++ translatorActions ++ printerActions ++ readerActions

    

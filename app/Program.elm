port module Program exposing (..)

import Html exposing (Html)
import Http
import Json.Decode as Json
import Json.Encode as Jsone
import Article exposing (..)
import View exposing (view)


main = Html.programWithFlags { init = init, update = update, view = view, subscriptions = subscriptions }

port buyArticle : Json.Value -> Cmd msg
port previewArticle : Json.Value -> Cmd msg
port uploadArticle : Json.Value -> Cmd msg
port requestEditors : Json.Value -> Cmd msg
port requestTranslators : Json.Value -> Cmd msg
port requestPrinters : Json.Value -> Cmd msg
port publishArticle : Json.Value -> Cmd msg
port bidForEditing : Json.Value -> Cmd msg
port bidForTranslating : Json.Value -> Cmd msg
port bidForPrinting : Json.Value -> Cmd msg
port submitForApproval : Json.Value -> Cmd msg
port deleteArticle : Json.Value -> Cmd msg
port printComplete : Json.Value -> Cmd msg
port printInProgress : Json.Value -> Cmd msg
port translationComplete : Json.Value -> Cmd msg
port translationInProgress : Json.Value -> Cmd msg
port editComplete : Json.Value -> Cmd msg
port editInProgress : Json.Value -> Cmd msg
port approvePrint : Json.Value -> Cmd msg
port approveEdits : Json.Value -> Cmd msg
port approveTranslation : Json.Value -> Cmd msg
                     

init : Flags -> (Model, Cmd Msg)
init flags = (Model [] [] [] [] "Shekar Mantha", Http.send ArticlesRetrieved <| getArticles flags.baseUrl )

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        ArticlesRetrieved (Ok articles) -> ( { model | articles = articles }, Cmd.none)

        ArticlesRetrieved (Err err) -> Debug.log("Error getting articles: " ++ toString err) (model, Cmd.none)

        UploadArticle user -> (model, uploadArticle <| userEncoder user)

        RequestEditors article -> (model, requestEditors <| articleEncoder article)

        RequestTranslators article -> (model, requestTranslators <| articleEncoder article)

        RequestPrinters article -> (model, requestPrinters <| articleEncoder article)

        PublishArticle article -> (model, publishArticle <| articleEncoder article)

        BidForEditing article -> (model, bidForEditing <| articleEncoder article)

        BidForTranslating article -> (model, bidForTranslating <| articleEncoder article)

        BidForPrinting article -> (model, bidForPrinting <| articleEncoder article)

        SubmitForApproval article -> (model, submitForApproval <| articleEncoder article)

        PreviewArticle article -> (model, previewArticle <| articleEncoder article )

        BuyArticle article -> (model, buyArticle <| articleEncoder article )

        DeleteArticle article -> (model, deleteArticle <| articleEncoder article )

        ApproveEdits article -> (model, approveEdits <| articleEncoder article )
                                
        ApproveTranslation article -> (model, approveTranslation <| articleEncoder article )
                                      
        ApprovePrint article -> (model, approvePrint <| articleEncoder article )
                                
        EditInProgress article -> (model, editInProgress <| articleEncoder article )
                                  
        EditComplete article -> (model, editComplete <| articleEncoder article )
                                
        TranslationInProgress article -> (model, translationInProgress <| articleEncoder article )
                                         
        TranslationComplete article -> (model, translationComplete <| articleEncoder article )
                                       
        PrintInProgress article -> (model, printInProgress <| articleEncoder article )
                                   
        PrintComplete article -> (model, printComplete <| articleEncoder article )
                                 
        
subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.none


getArticles : String -> Http.Request (List Article)
getArticles baseUrl =
           Http.get (baseUrl ++ "/articles") (Json.list articleDecoder)
               

getUsers : String -> Http.Request (List User)
getUsers baseUrl =
           Http.get (baseUrl ++ "/users") (Json.list userDecoder)


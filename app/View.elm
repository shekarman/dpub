module View exposing (view)

import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Article exposing (..)


view : Model -> Html Msg
view model =
    div []
        [
        h2 [] [ text "Articles" ]
        , div [] [ button [ onClick <| UploadArticle (getUserByName model.users model.currentUserName) ] [ text "Upload" ] ]
        , div [] <| List.map (\article -> articleRow model article) model.articles
        ]

getEditingButtons : Article -> List Editor -> List (Html Msg)
getEditingButtons article editors =
    if List.length editors /= 0 then
        [
         button [ onClick <| RequestEditors article ] [ text "More editing services... " ]
         , button [ onClick <| ApproveEdits article ] [ text "Approve edits... " ]
        ]
    else
        [
         button [ onClick <| RequestEditors article ] [ text "Editing services... " ]
        ]
        

getPrintingButtons : Article -> List Printer -> List (Html Msg)
getPrintingButtons article printers =
    if List.length printers /= 0 then
        [
         button [ onClick <| RequestPrinters article ] [ text "More printing services... " ]
        , button [ onClick <| ApprovePrint article ] [ text "Approve print copy... " ]
        ]
    else
        [
         button [ onClick <| RequestPrinters article ] [ text "Printing services... " ]
        ]

        
getAuthorButtons : Article -> String -> List Editor -> List Printer -> List (Html Msg)
getAuthorButtons article currentUserName editors printers =
    if article.author /= currentUserName then
        []
    else 
        case article.articleState of
            EMPTY ->
                []
            WRITING ->
                [
                 button [ onClick <| RequestEditors article ] [ text "Editing services... " ]
                , button [ onClick <| DeleteArticle article ] [ text "Delete article... " ]
                ]
            EDITING ->
                (getEditingButtons article editors)
                ++ [
                 button [ onClick <| DeleteArticle article ] [ text "Delete article... " ]
                ]
                        
            EPUBLISHED ->
                [
                 button [ onClick <| RequestPrinters article ] [ text "Revoke from sale ... " ]
                 , button [ onClick <| DeleteArticle article ] [ text "Delete article... " ]
                ]

            PRINTING ->
                (getPrintingButtons article printers)
                ++ [
                 button [ onClick <| DeleteArticle article ] [ text "Delete article... " ]
                ]
                

            PUBLISHED ->
                [
                 button [ onClick <| RequestPrinters article ] [ text "Revoke from sale ... " ]
                 , button [ onClick <| DeleteArticle article ] [ text "Delete article... " ]
                ]
                    
            DELETED ->
                [
                 button [ onClick <| RequestPrinters article ] [ text "Recover ... " ]
                ]


getEditorButtons : Article -> Int -> List Editor -> List (Html Msg)
getEditorButtons article userID editors =
    let
        editor = List.filter (\editor -> editor.userID == userID) editors
    in
        if List.length editor /= 0 then
            [
             button [ onClick <| EditInProgress article ] [ text "Mark edit in progress... " ]
            , button [ onClick <| EditComplete article ] [ text "Edit complete ... " ]
            ]
        else
            []

getPrinterButtons : Article -> Int -> List Printer -> List (Html Msg)
getPrinterButtons article userID printers =
    let
        printer = List.filter (\printer -> printer.userID == userID) printers
    in
        if List.length printer /= 0 then
            [
             button [ onClick <| PrintInProgress article ] [ text "Mark printing in progress... " ]
            , button [ onClick <| PrintComplete article ] [ text "Print complete ... " ]
            ]
        else
            []

getReaderButtons : Article -> String -> List (Html Msg)
getReaderButtons article userName =
    if article.author == userName then
        []
    else 
        [
         button [ onClick <| PreviewArticle article ] [ text "Preview..." ]
        , button [ onClick <| BuyArticle article ] [ text "Buy..." ]
        ]
        
articleRow : Model -> Article -> Html Msg
articleRow model article =
    let
        editors = model.editors
        printers = model.printers
        user = getUserByName model.users model.currentUserName
    in
        div [ class "article" ]
            [ div [ class "article__title" ] [ text article.title ]
            , span [] [ em [] [ text article.author ] ]
            , p []
                [ span [] [ text <| article.freeContentAddress ++ ", " ]
                ]
            , p []
                [ em [] [ text "Rating " ]
                , span [] [ text (toString article.rating) ]
                , em [] [ text " State " ]
                , span [] [ text (toString article.articleState) ]
                ]
            , div []
                (getAuthorButtons article model.currentUserName editors printers
                ++ getEditorButtons article user.userID  editors
                ++ getPrinterButtons article user.userID printers
                ++ getReaderButtons article model.currentUserName)
            ]
        

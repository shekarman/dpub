﻿using System;
using System.Data;
using System.Data.SQLite;
using log4net;
using System.Collections.Generic;

namespace dpub_schema
{
    public class SQLiteInterface
    {
        private string dbConnection;
        private ILog logger = LogManager.GetLogger("SQLiteInterface");

        public SQLiteInterface(string inputFile) {
            dbConnection = string.Format("DataSource={0}", inputFile);
        }

        public SQLiteInterface(Dictionary<string, string> connectionOptions) {
            string s = "";

            foreach (KeyValuePair<string, string> row in connectionOptions) {
                s += string.Format("{0} = {1}; ", row.Key, row.Value);
            }
            s = s.Trim().Substring(0, s.Length - 1);
            dbConnection = s;
        }

        private int executeNonQuery(string sql) {
            int rowsUpdated = 0;

            using (SQLiteConnection cnn = new SQLiteConnection(dbConnection)) {
                cnn.Open();
                using (SQLiteCommand command = new SQLiteCommand(cnn)) {
                    command.CommandText = sql;
                    logger.Info("sql-command: " + command.CommandText);
                    rowsUpdated = command.ExecuteNonQuery();
                }
                cnn.Close();
            }
            return rowsUpdated;
        }

        public string executeScalar(string sql) {
            Object value = null;
            using (SQLiteConnection connection = new SQLiteConnection(dbConnection)) {
                connection.Open();
                using (SQLiteCommand command = new SQLiteCommand(connection)) {
                    command.CommandText = sql;
                    logger.Info("sql-command: " + command.CommandText);
                    value = command.ExecuteScalar();
                    connection.Close();
                    if (value != null) {
                        return value.ToString();
                    }
                }
            }
            return (value == null) ? null : value.ToString();
        }

        public bool createTable(string tableName, Dictionary<string, string> fields) {
            string vals = "";

            try {
                if (fields.Count < 1) {
                    logger.Info("Cannot create an empty table.");
                    return false;
                }
                int i = 0;
                foreach (KeyValuePair<string, string> val in fields) {
                    vals += string.Format(" {0} {1}", val.Key.ToString(), val.Value.ToString());
                    if (i < fields.Count - 1)
                        vals += ", ";
                    i++;
                }
                vals = vals.Substring(0, vals.Length);
                using (SQLiteConnection connection = new SQLiteConnection(dbConnection)) {
                    connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(connection)) {
                        command.CommandText = string.Format("create table if not exists {0} ({1});", tableName, vals);
                        logger.Info("sql-command: " + command.CommandText);
                        command.ExecuteNonQuery();
                    }
                }
            } catch (Exception e) {
                logger.Error(e.Message);
                logger.Error(e.StackTrace);
                return false;
            }
            return true;
        }

        public bool insert(string tableName, string[] fieldNames, Object[] values) {
            string s, f;
            SQLiteConnection connection;
            SQLiteCommand insertSQL;

            try {
                connection = new SQLiteConnection(dbConnection);
                connection.Open();

                s = string.Format("INSERT INTO {0} (", tableName);
                for (int i = 0; i < fieldNames.Length; i++) {
                    s += fieldNames[i];
                    if (i < fieldNames.Length - 1)
                        s += ", ";
                }
                s += ") VALUES (";
                for (int i = 0; i < fieldNames.Length; i++) {
                    s += string.Format("@param{0}", i);
                    if (i < fieldNames.Length - 1)
                        s += ", ";
                }
                s += ")";
                insertSQL = new SQLiteCommand(s, connection);
                insertSQL.CommandText = s;
                logger.Info(String.Format("sql-command {0}", insertSQL.CommandText));
                for (int i = 0; i < values.Length; i++) {
                    logger.Info(string.Format("values[{0}] = {1}", i, values[i]));
                }
                for (int i = 0; i < values.Length; i++) {
                    f = string.Format("@param{0}", i);
                    insertSQL.Parameters.Add(new SQLiteParameter(f, values[i].ToString()));
                }
                insertSQL.ExecuteNonQuery();
            } catch (Exception e) {
                logger.Error(e.Message);
                logger.Error(e.StackTrace);
                return false;
            }
            return true;
        }

        public DataTable getDataTable(string sql) {
            DataTable table = new DataTable();

            try {
                using (SQLiteConnection connection = new SQLiteConnection(dbConnection)) {
                    connection.Open();
                    using (SQLiteCommand command = new SQLiteCommand(connection)) {
                        command.CommandText = sql;
                        // logger.Info(String.Format("sql-command {0}", command.CommandText));
                        using (SQLiteDataReader reader = command.ExecuteReader()) {
                            table.Load(reader);
                            reader.Close();
                        }
                    }
                    connection.Close();
                }
            } catch (Exception e) {
                logger.Error("Exception fetching table with: " + sql);
                logger.Error(e.Message);
                logger.Error(e.StackTrace);
                return null;
            }
            return table;
        }

        public bool update(string tableName, string[] fieldNames, object[] values, string whereClause) {
            string s = "", f;
            SQLiteConnection connection;
            SQLiteCommand command = null;

            try {
                connection = new SQLiteConnection(dbConnection);
                connection.Open();
                command = new SQLiteCommand(connection);

                for (int i = 0; i < fieldNames.Length; i++) {
                    s += string.Format("{0} = @param{1}", fieldNames[i], i);
                    if (i < fieldNames.Length - 1)
                        s += ", ";
                }
                command.CommandText = string.Format("UPDATE {0} set {1} {2};", tableName, s, whereClause);
                for (int i = 0; i < values.Length; i++) {
                    f = string.Format("@param{0}", i);
                    command.Parameters.Add(new SQLiteParameter(f, values[i]));
                }
                command.ExecuteNonQuery();
            } catch (Exception e) {
                logger.Error("Exception executing update table with: " + command.CommandText);
                logger.Error(e.Message);
                logger.Error(e.StackTrace);
                return false;
            }
            return true;
        }

        public bool delete(string tableName, string whereClause) {
            Boolean returnCode = true;
            SQLiteCommand command = null;

            try {
                using (SQLiteConnection connection = new SQLiteConnection(dbConnection)) {
                    connection.Open();
                    using (command = new SQLiteCommand(connection)) {
                        command.CommandText = string.Format("DELETE FROM {0} {1};", tableName, whereClause);
                        logger.Info("sql-command: " + command.CommandText);
                        command.ExecuteNonQuery();
                    }
                }
            } catch (Exception e) {
                logger.Error("Exception executing delete table with: " + command.CommandText);
                logger.Error(e.Message);
                logger.Error(e.StackTrace);
                returnCode = false;
            }
            return returnCode;
        }

        public bool clearTable(string table) {
            try {
                logger.Info(String.Format("sql-command: delete from {0};", table));
                this.executeNonQuery(string.Format("delete from {0};", table));
                return true;
            } catch (Exception e) {
                logger.Error("Exception executing clear table");
                logger.Error(e.Message);
                logger.Error(e.StackTrace);
                return false;
            }
        }

        public bool clearDB() {
            DataTable tables;
            try {
                tables = this.getDataTable("select NAME from SQLITE_MASTER where type='table' order by NAME;");
                foreach (DataRow table in tables.Rows) {
                    if (table["NAME"].ToString() == "sqlite_sequence")
                        continue;
                    this.clearTable(table["NAME"].ToString());
                    logger.Info(String.Format("sql-command: drop table if exists {0};", table));
                    this.executeNonQuery(string.Format("drop table if exists {0};", table["NAME"]));
                }
                return true;
            } catch (Exception e) {
                logger.Error("Exception executing clearDB");
                logger.Error(e.Message);
                logger.Error(e.StackTrace);
                return false;
            }
        }
    }
}

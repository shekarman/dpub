﻿using System;
using System.Data;
using System.Data.SQLite;
using log4net;
using System.Collections.Generic;

namespace dpub_schema
{
    class DPubDatabase
    {
        private static string DATABASE_NAME = "DPubDatabase";
        private static SQLiteInterface dbInterface = new SQLiteInterface(DATABASE_NAME);
        private static ILog logger = LogManager.GetLogger("DPupDatabase");

        public enum ArticleState {EMPTY, WRITING, SOLICITING_EDITORS, EDITING, EDIT_COMPLETE, AWAIT_EDIT_APPROVAL, PUBLISHED, FORSALE, DELETED};
        public enum CostType {EDIT, TRANSLATE, PRINT, ISBN_REGISTER, RESELL, ADVERTISE, SALE_PRICE, DPUB_COMMISSION, OTHER};
        public enum CostUnit {COIN, FIAT_CURRENCY};
        public enum CoinType {NOT_APPLICABLE, BIT_COIN, ETHER, LITE_COIN, FILE_COIN, DPUB_COIN};

        private static void createTables() {
            dbInterface.createTable("Articles", new Dictionary<string, string>() {
                    {"ArticleID", "TEXT"},
                    {"Title", "TEXT"},
                    {"Author", "TEXT"},
                    {"FreeContentAddress", "TEXT"},
                    {"ContentAddress", "TEXT"},
                    {"Language", "TEXT"},
                    {"State", "INTEGER"},
                    {"Rating", "INTEGER"},
                });

            dbInterface.createTable("Reviews", new Dictionary<string, string>() {
                    {"ArticleID", "TEXT"},
                    {"SourceArticleID", "TEXT"},
                    {"Title", "TEXT"},
                    {"Author", "TEXT"},
                    {"ContentAddress", "TEXT"},
                    {"Language", "TEXT"},
                    {"State", "INTEGER"},
                });

            dbInterface.createTable("ArticleStateChange", new Dictionary<string, string>() {
                    {"ArticleID", "TEXT"},
                    {"ArticleState", "INTEGER"},
                    {"TimeStateChanged", "TEXT"}
                });

            dbInterface.createTable("ArticleCosts", new Dictionary<string, string>() {
                    {"ArticleID", "TEXT"},
                    {"CostType", "INTEGER"},
                    {"CostUnit", "INTEGER"},
                    {"CoinType", "INTEGER"},
                    {"Currency", "TEXT"},
                    {"CostValue", "TEXT"}
                });

            dbInterface.createTable("Authors", new Dictionary<string, string>() {
                    {"ArticleID", "TEXT"},
                    {"UserID", "TEXT"},
                });

            dbInterface.createTable("Editors", new Dictionary<string, string>() {
                    {"ArticleID", "TEXT"},
                    {"UserID", "TEXT"},
                });

            dbInterface.createTable("Translators", new Dictionary<string, string>() {
                    {"ArticleID", "TEXT"},
                    {"UserID", "TEXT"},
                });

            dbInterface.createTable("Buyers", new Dictionary<string, string>() {
                    {"ArticleID", "TEXT"},
                    {"UserID", "TEXT"},
                });

            dbInterface.createTable("Users", new Dictionary<string, string>() {
                    {"UserID", "TEXT"},
                    {"UserAddress", "TEXT"},
                    {"ArticlesWritten", "TEXT"},
                    {"ArticlesPublished", "TEXT"},
                    {"ArticlesEdited", "TEXT"},
                    {"ArticlesTranslated", "TEXT"},
                    {"ArticlesBought", "TEXT"},
                    {"UserBio", "TEXT"},
                    {"WritingCapabilities", "TEXT"},
                    {"EditingCapabilities", "TEXT"},
                    {"TranslationCapabilities", "TEXT"},
                    {"Languages", "TEXT"},
                });

            dbInterface.createTable("AccountsReceivable", new Dictionary<string, string>() {
                    {"UserID", "TEXT"},
                    {"AmountType", "INTEGER"},
                    {"AmountUnit", "INTEGER"},
                    {"CoinType", "INTEGER"},
                    {"Currency", "INTEGER"},
                    {"Amount", "REAL"},
                });

            dbInterface.createTable("AccountsPayable", new Dictionary<string, string>() {
                    {"UserID", "TEXT"},
                    {"AmountType", "INTEGER"},
                    {"AmountUnit", "INTEGER"},
                    {"CointType", "INTEGER"},
                    {"Currency", "INTEGER"},
                    {"Amount", "REAL"},
                });
        }

        public static void Main(string[] args) {
            Console.WriteLine("Creating data tables.");
            createTables();
            Console.WriteLine("Created data tables.");
        }
    }
}

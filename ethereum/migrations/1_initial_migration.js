var Migrations = artifacts.require("./Migrations.sol");
var Book = artifacts.require("./Book.sol");

module.exports = function(deployer) {
    deployer.deploy(Migrations);
    deployer.deploy(Book);
};

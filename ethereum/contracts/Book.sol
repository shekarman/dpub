pragma solidity ^0.4.0;

// import DateTimeApi;

contract Book {
    enum Currency {ETHER_WEI, LOCAL_CURRENCY, BTC}
    enum BookState {EMPTY, WRITING, SOLICITING_EDITORS, EDITING, EDIT_COMPLETE, AWAIT_EDIT_APPROVAL, PUBLISHED, FORSALE, DELETED}
    enum Mode {AUDIO, TEXTUAL, VIDEO}

    struct Content { 
        bytes synopsis;
        bytes summary;
        bytes body;
        bytes cover;
    }

    // Even though the same book is translated into multiple languages, it should be stored as separate book objects.
    // Somehow, we have to be able to relate a book in multiple languages so multilingual readers can choose a book in a
    // different language when it is not available in their language of choice.

    // For now, we store the locations of the summary, synopsis, body, cover and audio separately. It may be possible
    // to define a format such that we can parse this information and store only one location in the contract. This may
    // be actually more secure.

    // We should use the bookID as the primary key. Also, remember searching by title would yield multiple results but searching
    // by the following four attributes should yield an unique bookID: author, title, language and modality.

    BookState public state;
    string public title;
    address public author;
    address public content;
    bytes2 public language;
    address[3] public editors;
    uint editRoyalty;
    Currency public currency;
    uint public price;
    uint public createdTime;
    uint public editedTime;
    uint public publishedTime;
    uint public forSaleTime;
    uint public sales;
    uint public views;
    uint public read;
    uint public reviews;

    mapping(address => Content) contentMapping;

    function Book() public {
        state = BookState.EMPTY;
    }

    function writeBook(string _title, address _content, bytes2 _language) public returns (int) {
        if (state != BookState.EMPTY)
            return -1;
        title = _title;
        author = msg.sender;
        createdTime = now;
        content = _content;
        language = _language;
        state = BookState.WRITING;
        return 0;
    }

    function submitForEditing(uint _royalty) public {
        if (msg.sender != author)
            return;
        if (_royalty < 0.0)
            return;
        if (state != BookState.WRITING)
            return;
        editRoyalty = _royalty;
        state = BookState.SOLICITING_EDITORS;
    }

    function bidForEditing(uint _royalty) public {
        if (msg.sender == author)
            return;
        if (_royalty > editRoyalty)
            return;
        if (state != BookState.SOLICITING_EDITORS)
            return;
        if (editors[0] == author)
            editors[0] = msg.sender;
        else if (editors[1] == author)
            editors[1] = msg.sender;
        else if (editors[2] == author)
            editors[2] = msg.sender;
        else
            return;
    }

    function selectEditor(address editor) public {
        if (msg.sender != author)
            return;
        if (editors[0] == editor || editors[1] == editor || editors[2] == editor) {
            editors[0] = editor;
            editors[1] = author;
            editors[2] = author;
        }
        state = BookState.EDITING;
    }

    function editComplete() public {
        if (msg.sender != editors[0])
            return;
        if (state != BookState.EDITING)
            return;
        editedTime = now;
        state = BookState.AWAIT_EDIT_APPROVAL;
    }

    function requestMoreEdits() public {
        if (msg.sender != author)
            return;
        if (state != BookState.AWAIT_EDIT_APPROVAL)
            return;
        state = BookState.EDITING;
    }

    function editAccepted() public {
        if (msg.sender != author)
            return;
        if (state != BookState.AWAIT_EDIT_APPROVAL)
            return;
        state = BookState.EDIT_COMPLETE;
    }

    function publish(uint _price) public {
        if (msg.sender != author)
            return;
        if (state != BookState.EDIT_COMPLETE)
            return;
        price = _price;
        publishedTime = now;
        state = BookState.PUBLISHED;
    }

    function sell() public {
        if (msg.sender != author)
            return;
        if (state != BookState.PUBLISHED)
            return;
        state = BookState.FORSALE;
    }

    function buy() public {
        if (msg.sender == author)
            return;
        if (state != BookState.FORSALE)
            return;
        if (msg.sender.balance < price)
            return;
        msg.sender.transfer(price);
        sales = sales + 1;
    }
}
